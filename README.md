# app-weather

Por Albano Fuentes Maya.

## Correr aplicación

### `npm start`

Abrirá [http://localhost:3000](http://localhost:3000) para verla en el browser.

### Aclaraciones.

Api-weather ofrece 2 endpoints gratuitos, current weather( que entrega el clima actual ) y 5 days / 3 hours weather forecast( pronóstico de 5 días por 3 horas), es decir desde la hora que se consulta y llegando a 4 dias mas. Sí estuviese disponible de forma gratuita, pasando como query param "cnt=6", se podria haber hecho de forma correcta. Por tal motivo "emulo" una situacion en la que hay 2 endpoints, uno con el clima actual y otro con el pronóstico de los siguientes 5 días. Verán que se ve repetido el día actual por tal motivo.