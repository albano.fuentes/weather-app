/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import { v4 as uuidv4 } from "uuid";
import Location from "./components/Location";
import "./App.css";
import logo from "./assets/afm-weather.jpg";

const api = {
  base: "https://api.openweathermap.org/data/2.5/",
  key: "a29afe1588a310ea1cd7354cf7c3f17c",
};

const epocToDate = (time) => {
  let date = new Date(0);
  date.setUTCSeconds(time);
  return date;
};

const dateBuilder = (d) => {
  const months = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];
  let date = d.getDate();
  let month = months[d.getMonth()];
  let year = d.getFullYear();

  return `${date} de ${month} de ${year}`;
};

const App = () => {
  const [query, setQuery] = useState("");
  const [weathers, setWeathers] = useState([]);
  const [error, setError] = useState(false);

  useEffect(async () => {
    const localCity = await getData();
    getWeather(localCity);
  }, []);

  const showError = () => {
    setError(true);
    setTimeout(() => setError(false), 5000);
  };

  const getData = async () => {
    const res = await fetch("http://ip-api.com/json/").then((res) =>
      res.json()
    );
    return res.city;
  };

  const getCurrentWeather = async (loc) => {
    let response = await fetch(
      `${api.base}weather?q=${loc}&units=metric&appid=${api.key}`
    ).then((res) => res.json());
    if (response.cod === "404") {
      showError();
      return null;
    } else {
      return response;
    }
  };

  const getForecastWeather = async (loc) => {
    let list = [];
    let response = await fetch(
      `${api.base}forecast?q=${loc}&units=metric&appid=${api.key}`
    ).then((res) => res.json());
    if (response.cod === "404") {
      showError();
      return null;
    } else {
      for (let i = 0, j = 0; i < 40; i += 8, j++) {
        list[j] = response.list[i];
      }
      return list;
    }
  };

  const getWeather = async (loc) => {
    const cw = await getCurrentWeather(loc);
    const fw = await getForecastWeather(loc);

    if (!cw || !fw) {
      showError();
      return null;
    } else {
      const currentLocation = convertToDTO(cw, fw);
      setWeathers([...weathers, currentLocation]);
    }
  };

  const capitalizeSentence = (sentence) => {
    const words = sentence.split(" ");

    return words
      .map((word) => {
        return word[0].toUpperCase() + word.substring(1);
      })
      .join(" ");
  };

  const convertToDTO = (cw, fw) => {
    let cwObj = {};
    const nextDays = [];
    const days = [
      "Domingo",
      "Lunes",
      "Martes",
      "Miércoles",
      "Jueves",
      "Viernes",
      "Sábado",
    ];
    let date = epocToDate(cw.dt);
    let day = days[date.getDay()];

    cwObj.day = day;
    cwObj.date = dateBuilder(date);
    cwObj.location = cw.name;
    cwObj.temp = Math.round(cw.main.temp);
    cwObj.description = capitalizeSentence(cw.weather[0].description);

    fw.forEach((w) => {
      date = epocToDate(w.dt);
      day = days[date.getDay()];
      let obj = {};
      obj.day = day;
      obj.temp = Math.round(w.main.temp);
      nextDays.push(obj);
    });
    cwObj.nextDays = nextDays;
    cwObj.id = uuidv4();

    return cwObj;
  };

  const search = ({ key }) => {
    if (key === "Enter" && query.trim() !== "") {
      if (weathers.length < 5) getWeather(query);
      setQuery("");
    }
  };

  const removeWeather = (id) => {
    const filteredWeathers = weathers.filter((w) => {
      return w.id !== id;
    });
    setWeathers(filteredWeathers);
  };

  return (
    <div>
      <main>
        <nav className="navbar navbar-dark bg-dark justify-content-between">
          <div className="form-inline mx-auto">
            <Row>
              <Col className="mr-1 ">
                <input
                  type="search"
                  className="form-control mr-sm-2 "
                  aria-label="Buscar"
                  placeholder="Buscar..."
                  onChange={(e) => setQuery(e.target.value)}
                  value={query}
                  onKeyPress={search}
                />
              </Col>
              <Col>
                <button
                  className="btn btn-outline-success my-2 my-sm-0"
                  onClick={(e) => search({key:'Enter'})}
                >
                  Buscar
                </button>
              </Col>
            </Row>
          </div>
          <a href="https://www.linkedin.com/in/albanofuentesmaya/" target="_blank" rel="noreferrer" >
            <div className="navbar-brand">
            App Weather
            <img className="mr-1" src={logo} alt="profile" width="64" height="64" />
            </div>
          </a>
        </nav>
        {error && (
          <div className="alert alert-danger " role="alert">
            No se encontro el lugar!
          </div>
        )}
        <Container>
          {weathers &&
            weathers.map((weather) => (
              <Row>
                <Col>
                  <Location weather={weather} removeWeather={removeWeather} />
                </Col>
              </Row>
            ))}
        </Container>
      </main>
    </div>
  );
};

export default App;
