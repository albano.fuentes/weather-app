import React, { Fragment } from "react";
import "../Location.css";

const ForecastDays = ({ weather }) => {
  return (
    <Fragment>
      <div class="weakly-weather-item">
        <p class="mb-0"> {weather.day} </p>{" "}
        <i class="mdi mdi-weather-cloudy"></i>
        <p class="mb-0"> {weather.temp}° </p>
      </div>
    </Fragment>
  );
};

export default ForecastDays;
