import React, { Fragment } from "react";
import ForecastDays from "./ForecastDays";
import { Button } from "react-bootstrap";

import "../Location.css";

const Location2 = ({ removeWeather, weather }) => {
  if (weather)
    return (
      <Fragment>
        <div class="page-content page-container" id="page-content">
          <div >
            <div class="row container d-flex justify-content-center">
              <div class="col-lg-8 grid-margin stretch-card">
                <div class="card card-weather">
                  <div class="card-body">
                    <div class="weather-date-location">
                      <h3>{weather.day}</h3>
                      <p class="text-gray">
                        {" "}
                        <span class="weather-date">{weather.date}</span>{" "}
                        <span class="weather-location">{weather.location}</span>{" "}
                      </p>
                    </div>
                    <div class="weather-data d-flex">
                      <div class="mr-auto">
                        <h4 class="display-3">
                          {weather.temp}
                          <span class="symbol">°</span>C
                        </h4>
                        <p> {weather.description} </p>
                      </div>
                    </div>
                  </div>
                  <div class="card-body p-0">
                    <div class="d-flex weakly-weather">
                      {weather.nextDays.map((weather) => (
                        <ForecastDays weather={weather} />
                      ))}
                    </div>
                  </div>
                </div>
                <Button
                  size="lg"
                  variant="danger"
                  onClick={() => removeWeather(weather.id)}
                >
                  Borrar
                </Button>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
};

export default Location2;
